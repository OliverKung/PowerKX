# STlinkV2.1 PowerKX™ Edition
## 说明
使用官方STlinkV2.1修改,采用支持PowerKX™的MX1.25接口进行下载.
同时集成了USART-M接口用于USART
## 项目来源
本项目原理图来自ST官方
本项目初版无TypeCPCB版图来自开源项目 https://github.com/leiyitan/Stlink_V2.1_PCB