# MotherBoard开发板说明
开发者:OliverKung
## 板载系统
- 电源管理系统
    - FBR+7805
- IO全引出
- 一个2.54mm LA接口(Logic Analyzer)
- 1.25mm基于PowerKX V1.0标准的外置接口
## 版本更迭
- V1.0 基本的母板,采用DC Socket进行功率的输入
## 已知问题
- 后悔了,不该用DC Socket的,这东西适合生产并不适合开发
## 后续修改
- [ ] 加入一个OLED12864接口,以减少OLED12864的使用,降低开发成本
- [ ] 加入一个编码器接口,引入输入设备,方便调试
- [ ] 将供电修改为USB-TypeC,并安装一个可插拔的STLink V2.1,用于供电和下载和USART


    