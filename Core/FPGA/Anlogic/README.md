# EG4A20开发板说明
开发者:OliverKung
## IC说明
- 19600个LUT
- 3个PLL
## 板载系统
- 必要的
    - 电源管理系统
        - 低压电源系统:EA3036 3路PMU
        - 高压电源系统:78SW05模块,平接
    - W25Q64 Flash存储器
    - 50MHz时钟
    - JTAG下载器
- 可选的
    - TL431外置基准
    - 两个用户按键
    - 一个用户LED
    - 2路PWM-DAC,without buffer
## 版本更迭
- V1.0 基本四层板试水
## 后续可能修改
1. 将DAC改为独立eDAC或者PWM+R2R DAC,with buffer
2. 优化下载/调试流程

    